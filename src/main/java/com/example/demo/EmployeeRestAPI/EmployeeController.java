package com.example.demo.EmployeeRestAPI;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
    public class EmployeeController {
        @GetMapping("/employees")
        public ArrayList<Employee> getEmployees() { 
            ArrayList<Employee> employees = new ArrayList<Employee>();

            Employee employees1 = new Employee(1 , "Giang","Tôn" , 5000);
            Employee employees2 = new Employee(2 , "Ninh","Trần" , 6000);
            Employee employees3 = new Employee(3 , "Thành","Lê" , 8000);

            employees.add(employees1);
            employees.add(employees2);
            employees.add(employees3);

            return employees;
    }

}
